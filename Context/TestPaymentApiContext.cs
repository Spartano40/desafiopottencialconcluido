using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Context
{
    public class TestPaymentApiContext : DbContext
    {
        public TestPaymentApiContext(DbContextOptions<TestPaymentApiContext>options) : base (options)
        {

        }

        public DbSet<Item> Items { get; set; }
        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Vendedor> Vendedors { get; set; }

    }
}